Thank you for filing a bug report for track.easterbunny.cc. Please fill out this issue template so that we can investigate and fix the bug you reported as fast as possible.

## Checklist
- [ ] I've made sure that I have the latest code on my machine
- [ ] I've looked in existing issues to make sure this issue is unique
- [ ] I've read the code of conduct
- [ ] This is not a security related issue, and can be publicly disclosed.

## Local Environment
Please describe your local environment. We've provided some prefills that are necessary to help us troubleshoot the issue.

If you are using Windows, make sure you provide us with detailed version info (such as Windows 10 1809, or Windows 7 SP1), instead of just Windows 10.

- Browser: `<Browser name>`, version `XX.X.X`
- OS: `<OS name>`, version `XX.X.X`
- Screen resolution: `XXXX by XXXX`
- Python version: `X.X.X`
- Web server: `Apache, Nginx`
- Web server version: `X.X.X`

## pip3 freeze output
On your machine, please run the command `pip3 freeze`. Then paste the output below.

```
Paste your pip3 freeze output here.
```

## Expected results
Please describe what you expected to occur here.

## Actual results
Please describe what actually happened (the bug).

## Steps to Reproduce
Please accurately describe how to reproduce the issue.

## Error Messages
Please put down the error messages, if possible, that result from this bug. This includes console output.

```
Put your console output here, in this code window.
```
