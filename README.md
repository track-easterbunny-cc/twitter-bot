# TEBCC v6 Twitter Bot
This is the code that powers the Twitter bot that runs on the @bunny_updates account.

# Important notice
This bot is meant to work with the Twitter API v1 (which is now shut down) using the Twython library. I can make no guarantee that this code will work for the new Twitter API v2, rate limits, etc. However, I am providing this code nonetheless.

# Installation and setup
The Twitter Bot requires Python 3 and only one custom library - Twython, make sure you install that via PIP.

Otherwise, there's a config file that I'll go over.

# Config file
There's two config files - config.ini and config_dev.ini. These are identical files but you can put different credentials in each if you'd like to test the bot before a full run.

## TWITTER section
Options are pretty explanatory - put in your consumer key/secret in addition to your access token/secret.

## BOT section
The first two options are pretty self explanatory, it's the full file path to where the route/datarow files are.

The `pt_endrow` option describes the ending row of pre-tracking. You will need to manually specify this, the script will not figure it out for you automatically.

The `max_baskets_delivered` and `max_baskets_delivered_under` options are to ensure that the twitter bot handles the last few amounts of baskets correctly during tracking. You want to set the max_baskets_delivered option to whatever the highest amount of baskets you'll ultimately have in your route (rounded to 3 decimal places), and max_baskets_delivered_under to the next thing below that.

e.g. if your max baskets delivered is `6.120 billion`, then the under is `6.119 billion` and the twitter bot will keep using that even if the rounding should suggest that `6.120 billion` should be used. this works best when you have a route that ends with at least 6 zeros on the end.

# Operation
To start, make sure that you modify datarow.txt to show 0 in it.

You'll want to feed the Twitter bot a v6 route called route.json in the same directory.

Then, run main.py every minute (via cron or something). The script will automatically tweet as necessary. It may tweet out a status update at the countdown point, so just be careful with that.

That's really it, the Twitter Bot is pretty simple otherwise.

# License
The Twitter Bot is licensed under the MIT License.
