# track.easterbunny.cc Twitter Bot Changelog
This changelog lists all the changes made to the Twitter Bot with each new release.

## Version 3.0.0 (track.easterbunny.cc v6.1.6)
* Adds support for v6-style files

## Version 2.0.0 (track.easterbunny.cc v5.5.1)
* Adds support for v5-style files
* Adds distance travelled support

## Version 1.0.0 (track.easterbunny.cc v4.5.4)
* Initial open-source release
* Adds configuration variables for easy bot configuration
* Adds in max basket limiter
