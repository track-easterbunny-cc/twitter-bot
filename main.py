# TEBCC Twitter Bot v3.0.0
# Copyright (c) 2023 track.easterbunny.cc
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import time
import json
import sys
import datetime
from twython import Twython
from configparser import ConfigParser

# Configuration variables - check the Wiki for more information
cp = ConfigParser()
cp.read("config_dev.ini")
routefile_path = cp.get("BOT", "routefile_path")
datarow_path = cp.get("BOT", "datarow_path")
pt_endrow = cp.getint("BOT", "pt_endrow")
max_baskets_limiter = True
max_baskets_delivered = cp.get("BOT", "max_baskets_delivered")
max_baskets_delivered_under = cp.get("BOT", "max_baskets_delivered_under")
consumer_key = cp.get("TWITTER", "consumer_key")
consumer_secret = cp.get("TWITTER", "consumer_secret")
access_token = cp.get("TWITTER", "access_token")
access_token_secret = cp.get("TWITTER", "access_token_secret")


def human_format(num):
    num = float('{:.4g}'.format(num))
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0

    returnnum = str(num) + " " + ['', 'thousand', 'million', 'billion', 'trillion'][magnitude]
    if returnnum == "0.0 ":
        returnnum = "0 "

    return returnnum


json_file = open(routefile_path)
data = json.load(json_file)
datarow_file = open(datarow_path, 'r').read()
datarow_file = int(datarow_file)

pt_endts = int(data['destinations'][pt_endrow]['unixarrival'])

current_timestamp = int(time.time()) + 1

#now = datetime.datetime.now(datetime.timezone.utc)
now = datetime.datetime.now()

row = 0
max_dr = len(data['destinations']) - 1

# Even with the v6 format, we still use the unix arrival.
for row in range(0, max_dr + 2):
    try:
        if data['destinations'][row]['unixarrival'] < current_timestamp:
            continue
        else:
            break
    except:
        if row == max_dr + 1:
            row = max_dr + 1

# Prevent tweet from going out if DR 0 unix arrival is greater than current timestamp
if data['destinations'][0]['unixarrival'] > current_timestamp:
    sys.exit()

if int(row) == int(datarow_file):
    sys.exit()

tweetstring = ""
formatted_time = now.strftime("%-I:%M %p EDT, %-m/%-d/%y")
tweetstring = tweetstring + "Status Update (" + formatted_time + "):\n"

# Do not tweet the countdown row
if int(row) == 0:
    sys.exit()

if int(row) <= pt_endrow:
    tweetstring = tweetstring + "Last seen: " + data['destinations'][row - 1]['city'] + "\n\n"
    pt_countdown = pt_endts - data['destinations'][row - 1]['unixarrival']

    m, s = divmod(pt_countdown, 60)
    h, m = divmod(m, 60)

    pt_countdown_str = ""

    if h == 0 and m != 1:
        pt_countdown_str = "The Easter Bunny takes off in " + str(m) + " minutes."
    elif h == 0 and m == 1:
        pt_countdown_str = "The Easter Bunny takes off in " + str(m) + " minute."
    elif h == 1 and m != 1:
        pt_countdown_str = "The Easter Bunny takes off in " + str(h) + " hour, " + str(m) + " minutes."
    elif h == 1 and m == 1:
        pt_countdown_str = "The Easter Bunny takes off in " + str(h) + " hour, " + str(m) + " minute."
    elif h != 1 and m == 1:
        pt_countdown_str = "The Easter Bunny takes off in " + str(h) + " hours, " + str(m) + " minute."
    elif h != 1 and m != 1:
        pt_countdown_str = "The Easter Bunny takes off in " + str(h) + " hours, " + str(m) + " minutes."

    tweetstring = tweetstring + pt_countdown_str
elif int(row) == max_dr + 1:
    tweetstring = tweetstring + "The Easter Bunny has safely returned back home after a 25-hour journey around the world, in which 6.120 billion baskets were delivered. \n\nThank you for tracking with us in 2022, and we hope you track with us next year in 2023!"
else:
    if (int(row) - 1) == pt_endrow:
        tweetstring = tweetstring + "The Easter Bunny has lifted off! \n"

    timedelay = int(data['destinations'][row]['unixarrival_v2'] - data['destinations'][row - 1]['unixdeparture'])

    if timedelay < 60:
        timedelay = str(timedelay) + " seconds"
    elif timedelay // 60 == 1 and timedelay % 60 == 1:
        timedelay = str(timedelay // 60) + " minute, " + str(timedelay % 60) + " second"
    elif timedelay // 60 == 1:
        timedelay = str(timedelay // 60) + " minute, " + str(timedelay % 60) + " seconds"
    elif timedelay % 60 == 1:
        timedelay = str(timedelay // 60) + " minutes, " + str(timedelay % 60) + " second"
    else:
        timedelay = str(timedelay // 60) + " minutes, " + str(timedelay % 60) + " seconds"

    baskets_delivered = human_format(data['destinations'][row - 1]['eggsdelivered'])
    baskets_delivered_array = baskets_delivered.split(" ")
    if max_baskets_limiter:
        if baskets_delivered == max_baskets_delivered:
            baskets_delivered = max_baskets_delivered_under
        else:
            # Is this maintainable code? No. Is it 4 edge cases? Yes. So deal with it.
            if len(baskets_delivered_array[0]) != 5 and baskets_delivered_array[0] != "0":
                if len(baskets_delivered_array[0]) == 1:
                    baskets_delivered_array[0] = baskets_delivered_array[0] + "0000"
                elif len(baskets_delivered_array[0]) == 2:
                    baskets_delivered_array[0] = baskets_delivered_array[0] + "000"
                elif len(baskets_delivered_array[0]) == 3:
                    baskets_delivered_array[0] = baskets_delivered_array[0] + "00"
                elif len(baskets_delivered_array[0]) == 4:
                    baskets_delivered_array[0] = baskets_delivered_array[0] + "0"
            baskets_delivered = baskets_delivered_array[0] + " " + baskets_delivered_array[1]
    else:
        # Is this maintainable code? No. Is it 4 edge cases? Yes. So deal with it.
        if len(baskets_delivered_array[0]) != 5 and baskets_delivered_array[0] != "0":
            if len(baskets_delivered_array[0]) == 1:
                baskets_delivered_array[0] = baskets_delivered_array[0] + "0000"
            elif len(baskets_delivered_array[0]) == 2:
                baskets_delivered_array[0] = baskets_delivered_array[0] + "000"
            elif len(baskets_delivered_array[0]) == 3:
                baskets_delivered_array[0] = baskets_delivered_array[0] + "00"
            elif len(baskets_delivered_array[0]) == 4:
                baskets_delivered_array[0] = baskets_delivered_array[0] + "0"
        baskets_delivered = baskets_delivered_array[0] + " " + baskets_delivered_array[1]

    distance_traveled_km = data['destinations'][row - 1]['distance-km']
    distance_traveled_mi = data['destinations'][row - 1]['distance-mi']

    tweetstring = tweetstring + "Last seen: " + data['destinations'][row - 1]['city'] + ", " + data['destinations'][row - 1]['region'] + "\n"
    tweetstring = tweetstring + "Next stop: " + data['destinations'][row]['city'] + ", " + data['destinations'][row]['region'] + " in " + timedelay + "\n\n"
    tweetstring = tweetstring + "Baskets delivered: " + baskets_delivered + "\n"
    #tweetstring = tweetstring + "Distance travelled: " + f"{distance_traveled_km:,.0f}" + " km"
    tweetstring = tweetstring + "Distance travelled: " + f"{distance_traveled_mi:,.0f}" + " mi (" + f"{distance_traveled_km:,.0f}" + " km)"
    #tweetstring = tweetstring + "Distance travelled: " + f"{distance_traveled_km:,.0f}" + " km (" + f"{distance_traveled_mi:,.0f}" + " mi)"

# Update the datarow before we tweet. This increases the chances of failure, however, we need to do this with longer
# stopping stops.
datarow_file = open(datarow_path, 'w')
datarow_file.write(str(row))

sleep_delay = data['destinations'][row - 1]['unixdeparture'] - data['destinations'][row - 1]['unixarrival']
twitter = Twython(consumer_key, consumer_secret, access_token, access_token_secret)
time.sleep(sleep_delay)
twitter.update_status(status=tweetstring)
