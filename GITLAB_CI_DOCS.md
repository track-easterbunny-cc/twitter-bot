# TEBCC Twitter Bot CI Pipeline Documentation
This document describes how the GitLab CI pipelines work for the Twitter Bot.

It's best you get familiar with the documentation of the main project's CI pipelines before reading this documentation.

# Before you run CI pipelines
I've intentionally made it such that the gitlab-ci file doesn't have the right name. You'll want to rename it to `.gitlab-ci.yml`.

I did this so it would avoid triggering pipelines.

# Note!
In the final LFTP step of most deploys - you'll see a directory called `/builds/tebcc/repo-name`. This is where gitlab stores the code on the runners. You'll have to adjust whatever comes after `/builds/` to the path of your repository.

# Stages
* Deploy

# Deploy (dev/prod)
In short, the deploy stage gets the latest route file from a specified URL (hardcoded into the CI file, you will need to change it).

Then, the script uploads all the files to the specified directory.

# GitLab CI variables
These are the variables you need to configure to make the pipelines run successfully.

When making these variables in GitLab settings, you can use the default values (unless otherwise noted)

You will find through these variables that security for these pipelines are awful, and you are more than welcome to try and improve it.

## HOST
The remote host for where to send the files to.

## PASSWORD
The password for a user that has access to the folder(s) that the twitter bot is getting deployed to (since SFTP uploading is used to get the files up to the remote server).

## USERNAME
The username of the remote user that has access to the folder(s) that the twitter bot is getting deployed to.

## SERVER_PATH
The path of where to deploy the prod twitter bot files to, with a trailing right slash at the end.

## SERVER_PATH_DEV
The path of where to deploy the dev twitter bot files to, with a trailing right slash at the end.

## SERVER_PORT
The SSH port of the remote server.
